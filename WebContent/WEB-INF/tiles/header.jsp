<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<div class="header">
	<div class="header-inner-div clear">
		<div class="header-logo">
			<a href="<c:url value='/' />"><img
				src="${pageContext.request.contextPath}/static/images/logo.png"
				title="OfferCrowd" /></a>
		</div>

		<div class="header-buttons">

			<sec:authorize access="!isAuthenticated()">
				<a href="<c:url value='/login' />"><button class="loginBtn">Login</button></a>
			</sec:authorize>

			<sec:authorize access="isAuthenticated()">
				<a href="<c:url value='/j_spring_security_logout' />"><button
						class="loginBtn">Logout</button></a>
			</sec:authorize>
		</div>

	</div>
</div>