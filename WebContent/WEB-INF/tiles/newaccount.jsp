<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="main-content">
	<header>
		<h2>
			<span>C</span>reate Account
		</h2>
	</header>

	<sf:form id="details"
		action="${pageContext.request.contextPath}/createaccount"
		method="post" commandName="user">

		<table class="formtable">
			<tr>
				<td class="label">Username:</td>
				<td class="input-td"><sf:input class="input-data"
						name="username" type="text" path="username" /><br> <sf:errors
						path="username" cssClass="error"></sf:errors></td>
			</tr>

			<tr>
				<td class="label">Name:</td>
				<td class="input-td"><sf:input class="input-data" name="name"
						type="text" path="name" /><br> <sf:errors path="name"
						cssClass="error"></sf:errors></td>
			</tr>

			<tr>
				<td class="label">Email:</td>
				<td class="input-td"><sf:input class="input-data" name="email"
						path="email" type="text" /><br> <sf:errors path="email"
						cssClass="error"></sf:errors></td>
			</tr>

			<tr>
				<td class="label">Password:</td>
				<td class="input-td"><sf:input class="input-data" id="password"
						name="password" path="password" type="password" /><br> <sf:errors
						path="password" cssClass="error"></sf:errors></td>
			</tr>

			<tr>
				<td class="label">Confirm Password:</td>
				<td class="input-td"><input class="input-data" id="confirmpass"
					name="confirmpass" type="password" /><br>
					<div id="matchpass"></div></td>
			</tr>

			<tr>
				<td class="label">Gender:</td>
				<td class="gender-td">
					<div class="gender-male">
						<sf:radiobutton class="male" value="male" name="sex" path="sex" />
						Male

					</div>
					<div class="gender-female">
						<sf:radiobutton class="female" value="female" name="sex" path="sex" />
						Female
					</div> <br> <sf:errors path="sex" cssClass="error"></sf:errors>
				</td>
			</tr>

			<tr>
				<td class="label">Country:</td>
				<td class="input-td" colspan="2"><sf:select id="country"
						class="country" path="country">
						<sf:option value="NONE" label="--- Select ---" />
						<sf:options items="${countryList}" />
					</sf:select><br> <sf:errors path="country" cssClass="error"></sf:errors></td>
			</tr>

			<tr>
				<td class="label"></td>
				<td>
					<div class="button-create">
						<input value="Create Account" type="submit" />
					</div>
					<div class="button-cancel">
						<a href='<c:url value="/" />'><input value="Cancel Account" /></a>
					</div>
				</td>
			</tr>
		</table>
	</sf:form>
</div>
