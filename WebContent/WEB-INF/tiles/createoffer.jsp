<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>


<script type="text/javascript">
<!--
	function onDeleteClick(event) {
		var doDelete = confirm("Are you sure you want to delete this offer?");
		if (doDelete == false) {
			event.preventDefault();
		}

	}

	function onReady() {
		$("#delete").click(onDeleteClick);
		autoHeight();
		$("container").find("textarea").keyup();
	}

	// auto adjust the height of
	function autoHeight() {
		$("#container").on("keyup", "textarea", function(e) {
			$(this).css("height", "auto");
			$(this).height(this.scrollHeight);
		});
	}

	$(document).ready(onReady);
//-->
</script>

<div class="main-content">
	
	<sf:form action="${pageContext.request.contextPath}/docreate"
		method="post" commandName="offer">

		<sf:input type="hidden" name="id" path="id" />

		<table class="newOffer">

			<tr>
				<td class="label">Your offer:</td>
				<td id="container" colspan="2"><sf:textarea rows="1"
						name="text" path="text" /><br> <sf:errors path="text"
						cssClass="error"></sf:errors></td>
			</tr>
			<tr>
				<td class="label">&nbsp;</td>
				<td class="buttonSD"><input value="Send" type="submit" /></td>
			</tr>
			<c:if test="${offer.id != 0}">
				<tr>
					<td class="label">&nbsp;</td>
					<td><input id="delete" name="delete" value="Delete"
						type="submit" /></td>
				</tr>
			</c:if>

		</table>

	</sf:form>

</div>


<%-- <sf:form action="${pageContext.request.contextPath}/docreate"
		method="post" commandName="offer">

		<sf:input type="hidden" name="id" path="id" />

		<table class="formtable">

			<tr>
				<td class="label">Your offer:</td>
				<td><sf:textarea rows="8" cols="16" name="text" path="text" /><br>
					<sf:errors path="text" cssClass="error"></sf:errors></td>
			</tr>
			<tr>
				<td class="label">&nbsp;</td>
				<td><input value="Update/Create Advert" type="submit" /></td>
			</tr>
			<c:if test="${offer.id != 0}">
				<tr>
					<td class="label">&nbsp;</td>
					<td><input id="delete" name="delete" value="Delete Advert"
						type="submit" /></td>
				</tr>
			</c:if>

		</table>

	</sf:form>  --%>
