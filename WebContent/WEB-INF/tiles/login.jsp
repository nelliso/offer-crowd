<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>


<div class="main-content">
	<script type="text/javascript">
		$(document).ready(function() {
			document.f.j_username.focus();
		});
	</script>
	<header>
		<h2>
			<span>L</span>og in here
		</h2>
	</header>
	<div class="login-div">

		<form class="login-form" name='f'
			action='${pageContext.request.contextPath}/j_spring_security_check'
			method='POST'>
			<table class="formtable">
				<tr>
					<td class="login-text">User:</td>
					<td class="login-input"><input type='text' name='j_username'
						value=''></td>
				</tr>

				<tr>
					<td class="login-text">Password:</td>
					<td class="login-input"><input type='password'
						name='j_password' /> <br /> <c:if test="${param.error != null }">
							<span class="error">Login failed. Check that your username
								and password are correct.</span>
						</c:if></td>
				</tr>

				<tr>
					<td class="login-text">Remember me:</td>
					<td><input type='checkbox' name='_spring_security_remember_me'
						checked="checked" /></td>
				</tr>

				<tr>
					<td class="login-text"></td>
					<td class="login-button"><input name="submit" type="submit"
						value="Login" /></td>
				</tr>
				<tr>
					<td class="login-text"></td>
					<td class="login-input" colspan='2'></td>
				</tr>
			</table>
		</form>
		<div>
			<table>
				<tr>
					<td class="create-text"></td>
					<td class="create-button"><a
						href="<c:url value="/newaccount" />"><input type="button"
							value="Create new account" /></a></td>
				</tr>
				<tr>
					<td class="login-text"></td>
					<td class="login-button" colspan='2'></td>
				</tr>

			</table>
		</div>
	</div>
</div>
