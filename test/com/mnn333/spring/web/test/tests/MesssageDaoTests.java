package com.mnn333.spring.web.test.tests;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mnn333.spring.web.dao.Message;
import com.mnn333.spring.web.dao.MessagesDao;
import com.mnn333.spring.web.dao.OffersDao;
import com.mnn333.spring.web.dao.User;
import com.mnn333.spring.web.dao.UsersDao;

@ActiveProfiles("dev")
@ContextConfiguration(locations = {
		"classpath:com/mnn333/spring/web/config/dao-context.xml",
		"classpath:com/mnn333/spring/web/config/security-context.xml",
		"classpath:com/mnn333/spring/web/test/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class MesssageDaoTests {
	@Autowired
	private OffersDao offersDao;

	@Autowired
	private MessagesDao messagesDao;
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private UsersDao usersDao;
	
	private User user1 = new User("johnwpurcell", "hellothere", "john@gmail.com", "Uganda", "male", "John Purcell", true, "ROLE_USER");
	private User user2 = new User("richardhannay", "the39steps", "richard@gmail.com", "Uganda", "male", "Richard Hannay", true, "ROLE_ADMIN");
	private User user3 = new User("suetheviolinins", "iloveviolins", "sue@gmail.com", "Uganda", "male", "Sue Black", true, "ROLE_USER");
	private User user4 = new User("rogerblake", "liberator", "roger@gmail.com", "Uganda", "male", "Roger Blake", false, "user");
	
	@Before
	public void init() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		
		jdbc.execute("delete from offers");
		jdbc.execute("delete from messages");
		jdbc.execute("delete from users");		
	}

	@Test
	public void testSave(){
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		
		Message message1 = new Message("Test Subject 1", "Test content1", "Isaac Newton", "isaac@caveofprogramming.com", user1.getUsername());
		messagesDao.saveOrUpdate(message1);
	}
	

}
