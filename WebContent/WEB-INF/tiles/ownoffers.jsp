<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>


<script type="text/javascript">
<!--
	function onDeleteClick(event) {

		var doDelete = confirm("Are you sure you want to delete this offer?");
		if (doDelete == false) {
			event.preventDefault();
		}

	}

	function onReady() {
		$("#delete").click(onDeleteClick);
	}

	$(document).ready(onReady);
//-->
</script>


<div class="main-content">

	<sf:form action="${pageContext.request.contextPath}/docreate"
		method="post" commandName="ownoffer">


		<table>
			<tr>
				<td>Edit your Offers</td>
			</tr>

			  <c:forEach var="offer" items="${ownoffer}">
				<tr>
					<td><c:out value="${offer.text}"></c:out></td>

					<td><input value="Edit" type="submit" /></td>

					<c:if test="${offer.id != 0 }">
						<td><input id="delete" name="delete" value="Delete"
							type="submit" /></td>
					</c:if>

				</tr>
			</c:forEach> 
		</table>

	</sf:form><%--  --%>

</div>
