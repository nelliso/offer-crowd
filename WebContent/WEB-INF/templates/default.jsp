<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title><tiles:insertAttribute name="title"></tiles:insertAttribute>
</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/main.css" />

<script type="text/javascript"
	src="${pageContext.request.contextPath}/static/script/jquery-11.2.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/static/script/main.js"></script>

<tiles:insertAttribute name="includes"></tiles:insertAttribute>

</head>
<body>
	<div>
		<tiles:insertAttribute name="header"></tiles:insertAttribute>
	</div>
	<div class="wrapper clear">

		<div class="content clear">
			<tiles:insertAttribute name="sidemenu"></tiles:insertAttribute>
			<tiles:insertAttribute name="messagesidemenu"></tiles:insertAttribute>
			<tiles:insertAttribute name="mainmenu"></tiles:insertAttribute>
		</div>

		<div>
			<tiles:insertAttribute name="footer"></tiles:insertAttribute>
		</div>
		
	</div>
</body>
</html>