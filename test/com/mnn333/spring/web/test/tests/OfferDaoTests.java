package com.mnn333.spring.web.test.tests;

import static org.junit.Assert.*;

import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mnn333.spring.web.dao.Offer;
import com.mnn333.spring.web.dao.OffersDao;
import com.mnn333.spring.web.dao.User;
import com.mnn333.spring.web.dao.UsersDao;

@ActiveProfiles("dev")
@ContextConfiguration(locations = {
		"classpath:com/mnn333/spring/web/config/dao-context.xml",
		"classpath:com/mnn333/spring/web/config/security-context.xml",
		"classpath:com/mnn333/spring/web/test/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class OfferDaoTests {
	@Autowired
	private OffersDao offersDao;

	@Autowired
	private UsersDao usersDao;

	@Autowired
	private DataSource dataSource;

	private User user1 = new User("johnwpurcell", "hellothere",
			"john@gmail.com", "Uganda", "male", "John Purcell", true, "ROLE_USER");
	private User user2 = new User("richardhannay", "the39steps",
			"richard@gmail.com", "Uganda", "male", "Richard Hannay", true, "ROLE_ADMIN");
	private User user3 = new User("suetheviolinins", "iloveviolins",
			"sue@gmail.com", "Uganda", "male", "Sue Black", true, "ROLE_USER");
	private User user4 = new User("rogerblake", "liberator", "roger@gmail.com", "Uganda", "male",
			"Roger Blake", false, "user");

	private Offer offer1 = new Offer(user1, "This is a test offer.");
	private Offer offer2 = new Offer(user2, "This is another test offer.");
	private Offer offer3 = new Offer(user3, "This is yet another test offer.");
	private Offer offer4 = new Offer(user4, "This is a tes offer once again.");
	private Offer offer5 = new Offer(user3,
			"Here is an interesting offer of some kind.");

	@Before
	public void init() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);

		jdbc.execute("delete from offers");
		jdbc.execute("delete from messages");
		jdbc.execute("delete from users");
	}
	
	@Test
	public void testDelete(){
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		
		offersDao.saveOrUpdate(offer1);
		offersDao.saveOrUpdate(offer2);
		offersDao.saveOrUpdate(offer3);
		offersDao.saveOrUpdate(offer4);
		offersDao.saveOrUpdate(offer5);
		
		Offer retrieved1 = offersDao.getOffers(offer2.getId());
		assertNotNull("Offer with ID " +  retrieved1.getId() + " should not be null (deleted, actual)", retrieved1);
		
		offersDao.delete(offer2.getId());
		
		Offer retrieved2 = offersDao.getOffers(offer2.getId());
		assertNull("Offer with ID " +  retrieved1.getId() + " should be null (deleted, actual)", retrieved2);
	}
	
	@Test
	public void testGetById(){
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		
		offersDao.saveOrUpdate(offer1);
		offersDao.saveOrUpdate(offer2);
		offersDao.saveOrUpdate(offer3);
		offersDao.saveOrUpdate(offer4);
		offersDao.saveOrUpdate(offer5);
		
		Offer retrieved1 = offersDao.getOffers(offer1.getId());
		assertEquals("Offers should match", offer1, retrieved1);
		
		Offer retrieved2 = offersDao.getOffers(offer4.getId());
		assertNull("Should not retrieve offer for disabled user.", retrieved2);
	}
	

	@Test
	public void testCreateRetrieve() {
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);

		offersDao.saveOrUpdate(offer1);
		offersDao.saveOrUpdate(offer2);
		offersDao.saveOrUpdate(offer3);
		offersDao.saveOrUpdate(offer4);
		offersDao.saveOrUpdate(offer5);

		List<Offer> offers = offersDao.getOffers();
		assertEquals("Should be four offers for enabled users.", 4,
				offers.size());

	}

	@Test
	public void testGetUsername() {
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);

		offersDao.saveOrUpdate(offer1);
		offersDao.saveOrUpdate(offer2);
		offersDao.saveOrUpdate(offer3);
		offersDao.saveOrUpdate(offer4);
		offersDao.saveOrUpdate(offer5);

		List<Offer> offers1 = offersDao.getOffers(user3.getUsername());
		assertEquals("Should be 2 offers for this user.", 2, offers1.size());

		List<Offer> offers2 = offersDao.getOffers("jdlkdjldk");
		assertEquals("Should be 0 offers for this user.", 0, offers2.size());

		List<Offer> offers3 = offersDao.getOffers(user1.getUsername());
		assertEquals("Should be 1 offers for this user.", 1, offers3.size());

	}

	@Test
	public void tesUpdate() {
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);

		offersDao.saveOrUpdate(offer1);
		offersDao.saveOrUpdate(offer2);
		offersDao.saveOrUpdate(offer3);
		offersDao.saveOrUpdate(offer4);
		offersDao.saveOrUpdate(offer5);

		offer3.setText("This offer has updated text.");
		offersDao.saveOrUpdate(offer3);

		Offer retrieved = offersDao.getOffers(offer3.getId());
		assertEquals("Retrieved offer should be updated.", offer3, retrieved);
		System.out.println(retrieved.getText());
	}
}
