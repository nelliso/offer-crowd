package com.mnn333.spring.web.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mnn333.spring.web.dao.FormValidationGroup;
import com.mnn333.spring.web.dao.Message;
import com.mnn333.spring.web.dao.User;
import com.mnn333.spring.web.service.UsersService;

@Controller
public class LoginController {

	@Autowired
	private UsersService usersService;

	/*
	 * public void setUsersService(UsersService usersService) {
	 * this.usersService = usersService; }
	 */

	@RequestMapping("/login")
	public String showLogin() {
		return "login";
	}

	@RequestMapping("/denied")
	public String showDenied() {
		return "denied";
	}

	@RequestMapping("/messages")
	public String showMessages() {
		return "messages";
	}

	@RequestMapping("/admin")
	public String showAdmin(Model model) {
		List<User> users = usersService.getAllUsers();
		model.addAttribute("users", users);
		return "admin";
	}

	@RequestMapping("/loggedout")
	public String showLoggedOut() {
		return "loggedout";
	}

	@RequestMapping("/newaccount")
	public String showNewAccount(Model model) {
		model.addAttribute("user", new User());
		modelList(model);
		return "newaccount";
	}

	private void modelList(Model model) {
	
		List<String> country = new ArrayList<String>();
		 country.add("Afghanistan"); 
		 country.add("�land Islands");
		 country.add("Albania"); 
		 country.add("Algeria");
		 country.add("American Samoa");
		 country.add("Andorra");
		 country.add("Angola");
		 country.add("Anguilla");
		 country.add("Antarctica");
		 country.add("Antigua and Barbuda");
		 country.add("Argentina");
		 country.add("Armenia");
		 country.add("Aruba");
		 country.add("Australia");
		 country.add("Austria");
		 country.add("Azerbaijan");
		 country.add("Bahamas");
		 country.add("Bahrain");
		 country.add("Bangladesh");
		 country.add("Barbados");
		 country.add("Belarus");
		 country.add("Belgium");
		 country.add("Belize");
		 country.add("Benin");
		 country.add("Bermuda");
		 country.add("Bhutan");
		 country.add("Bolivia");
		 country.add("Bosnia and Herzegovina");
		 country.add("Botswana");
		 country.add("Bouvet Island");
		 country.add("Brazil");
		 country.add("British Indian Ocean Territory");
		 country.add("Brunei Darussalam");
		 country.add("Bulgaria");
		 country.add("Burkina Faso");
		 country.add("Burundi");
		 country.add("Cambodia");
		 country.add("Cameroon");
		 country.add("Canada");
		 country.add("Cape Verde");
		 country.add("Cayman Islands");
		 country.add("Central African Republic");
		 country.add( "Chad");
		 country.add("Chile");
		 country.add("China");
		 country.add( "Christmas Island");
		 country.add("Cocos (Keeling) Islands");
		 country.add("Colombia");
		 country.add("Comoros");
		 country.add("Congo");
		 country.add("Congo, Democratic Republic");
		 country.add("Cook Islands");
		 country.add("Costa Rica");
		 country.add("Cote D'ivoire");
		 country.add("Croatia");
		 country.add("Cuba");
		 country.add("Cyprus");
		 country.add("Czech Republic");
		 country.add("Denmark");
		 country.add("Djibouti");
		 country.add("Dominica");
		 country.add("Dominican Republic");
		 country.add("Ecuador");
		 country.add("Egypt");
		 country.add("El Salvador");
		 country.add("Equatorial Guinea");
		 country.add("Eritrea");
		 country.add("Estonia");
		 country.add("Ethiopia");
		 country.add("Falkland Islands");
		 country.add("Faroe Islands");
		 country.add("Fiji");
		 country.add("Finland");
		 country.add("France");
		 country.add("French Guiana");
		 country.add("French Polynesia");
		 country.add("French Southern Territories");
		 country.add("Gabon");
		 country.add("Gambia");
		 country.add("Georgia");
		 country.add("Germany");
		 country.add("Ghana");
		 country.add("Greece");
		 country.add("Greenland");
		 country.add("Grenada");
		 country.add("Guadeloupe");
		 country.add("Guam");
		 country.add("Guatemala");
		 country.add("Guernsey");
		 country.add("Guinea");
		 country.add("Guinea-bissau");
		 country.add("Guyana");
		 country.add("Haiti");
		 country.add("Heard and Mcdonald Islands");
		 country.add("Vatican City State");
		 country.add("Honduras");
		 country.add("Hong Kong");
		 country.add("Hungary");
		 country.add("Iceland");
		 country.add("India");
		 country.add("Indonesia");
		 country.add("Iran");
		 country.add("Iraq");
		 country.add("Ireland");
		 country.add("Isle of Man");
		 country.add("Israel");
		 country.add("Italy");
		 country.add("Jamaica");
		 country.add("Japan");
		 country.add("Jersey");
		 country.add("Jordan");
		 country.add("Kazakhstan");
		 country.add("Kenya");
		 country.add("Kiribati");
		 country.add("Korea");
		 /*country.put();
		 country.put();
		 country.put();
		 country.put();
		 country.put();
		 country.put();
		 
		 "Korea, Republic of"
"Kuwait"
"Kyrgyzstan"
"Lao People's Democratic Republic"
"Latvia"
"Lebanon"
"Lesotho"
"Liberia"
"Libyan Arab Jamahiriya"
"Liechtenstein"
"Lithuania"
"Luxembourg"
"Macao"
"Macedonia"
"Madagascar"
"Malawi"
"Malaysia"
"Maldives"
"Mali"
"Malta"
"Marshall Islands"
"Martinique"
"Mauritania"
"Mauritius"
"Mayotte"
"Mexico"
"Micronesia"
"Moldova, Republic of"
"Monaco"
"Mongolia"
"Montenegro"
"Montserrat"
"Morocco"
"Mozambique"
"Myanmar"
"Namibia"
"Nauru"
"Nepal"
"Netherlands"
"Netherlands Antilles"
"New Caledonia"
"New Zealand"
"Nicaragua"
"Niger"
"Nigeria"
"Niue"
"Norfolk Island"
"Northern Mariana Islands"
"Norway"
"Oman"
"Pakistan"
"Palau"
"Palestinian Territory"
"Panama"
"Papua New Guinea"
"Paraguay"
 "Peru",
"Philippines"
 "Pitcairn"
"Poland"
"Portugal"
"Puerto Rico"
"Qatar"
 "Reunion"
 "Romania"
"Russian Federation"
"Rwanda"
"Saint Helena"
"Saint Kitts"
"Saint Lucia"
"Saint Pierre"
"Saint Vincent and The Grenadines"
 "Samoa"
"San Marino"
"Sao Tome and Principe"
"Saudi Arabia"
"Senegal"
"Serbia"
"Seychelles"
 "Sierra Leone"
 "Singapore"
"Slovakia"
"Slovenia"
 "Solomon Islands"
"Somalia"
"South Africa"
"South Georgia"
 "Spain"
"Sri Lanka"
"Sudan"
"Suriname"
"Svalbard and Jan Mayen"
"Swaziland"
"Sweden"
"Switzerland"
"Syrian Arab Republic"
"Taiwan"
"Tajikistan"
"Tanzania"
"Thailand"
"Timor-leste"
 "Togo
"Tokelau"
"Tonga"
"Trinidad and Tobago"
"Tunisia"
"Turkey"
"Turkmenistan"
"Turks and Caicos Islands"
"Tuvalu"*/
 country.add("Uganda");
/*"Ukraine"
"United Arab Emirates"
"United Kingdom"
"United States"
"Uruguay"
"Uzbekistan"
"Vanuatu"
"Venezuela"
"Viet Nam"
"Virgin Islands"
"Virgin Islands, U.S."
"Wallis and Futuna"
"Western Sahara"
"Yemen"
"Zambia"
"Zimbabwe"
		 */
		 model.addAttribute("countryList", country);
		 

	}

	@RequestMapping(value = "/createaccount", method = RequestMethod.POST)
	public String createAccount(
			@Validated(FormValidationGroup.class) User user,
			BindingResult result, Model model) {

		if (result.hasErrors() ) {
			modelList(model);
			return "newaccount";
		}

		if("NONE".equals(user.getCountry())){
			modelList(model);			
			result.rejectValue("country", "DuplicateKey.user.country");			
			return "newaccount";
		}
		
		user.setAuthority("ROLE_USER");
		user.setEnabled(true);
		
		if (usersService.exists(user.getUsername())) {
			result.rejectValue("username", "DuplicateKey.user.username");
			return "newaccount";
		}

		/*
		 * try { usersService.create(user); } catch (DuplicateKeyException e) {
		 * result.rejectValue("username", "DuplicateKey.user.username",
		 * "This username already exists!"); return "newaccount"; }
		 */
		
		usersService.create(user);

		return "accountcreated";
	}

	@RequestMapping(value = "/getmessages", method = RequestMethod.GET, produces = "application/json")
	
	public @ResponseBody Map<String, Object> getMessages(Principal principal) {
		List<Message> messages = null;

		if (principal == null) {
			messages = new ArrayList<Message>();
		} else {
			String username = principal.getName();
			messages = usersService.getMessages(username);
		}

		Map<String, Object> data = new HashMap<String, Object>();

		data.put("messages", messages);
		data.put("number", messages.size());

		return data;
	}

	@RequestMapping(value = "/sendmessage", method = RequestMethod.POST, produces = "application/json")
	
	public @ResponseBody Map<String, Object> sendMessage(Principal principal,
			@RequestBody Map<String, Object> data) {

		/*String text = (String) data.get("text");
		String name = (String) data.get("name");
		String email = (String) data.get("email");*/
		Integer target = (Integer) data.get("target");

		/*System.out.println("name: " + name + " email: " + email + " text: "
				+ text);*/

		Map<String, Object> rval = new HashMap<String, Object>();

		rval.put("success", true);
		rval.put("target", target);

		return rval;
	}
}
