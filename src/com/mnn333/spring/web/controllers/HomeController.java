package com.mnn333.spring.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mnn333.spring.web.service.OffersService;

@Controller
public class HomeController {

	@Autowired
	private OffersService offersService;

	// private static Logger logger = Logger.getLogger(HomeController.class);

	@RequestMapping("/")
	public String showHome(/* Model model, Principal principal */) {

		/*
		 * List<Offer> offers = offersService.getCurrent();
		 * model.addAttribute("offers", offers);
		 */
		// logger.info("Showing home page....");

		return "home";
	}

}
