<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>


<div class="main-content">
	<header>
		<h2><span>R</span>esults</h2>
	</header>
	<table class="formtable">
		<tr>
			<td>Name</td>
			<td>Email</td>
			<td>Text</td>
		</tr>

		<c:forEach var="offer" items="${offers}">
			<tr>
				<td><c:out value="${offer.user.name}"></c:out></td>
				<td><a
					href="<c:url value='/message?uid=${offer.user.username}' />">Contact</a></td>
				<td><c:out value="${offer.text}"></c:out></td>
			</tr>
		</c:forEach>
	</table>
</div>