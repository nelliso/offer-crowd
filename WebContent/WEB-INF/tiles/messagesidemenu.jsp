<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
	
<script type="text/javascript">
<!--
	function updateMessageLink(data) {
		$("#numberMessages").text(data.number);
	}

	function onLoad() {
		updatePage();
		window.setInterval(updatePage, 5000);

	}

	function updatePage() {
		$.getJSON("<c:url value="/getmessages" />", updateMessageLink)
	}

	$(document).ready(onLoad);
//-->
</script>	

<div class="sidemenu-sub">
	<div class="sidemenu-title">
		<div class="sidemenu-title-div">Menu</div>
	</div>
	<div class="sidemenu-tabs">
		<nav>
			<ul id="sidemenu-ul">

				<sec:authorize access="isAuthenticated()">
					<li class="side-tabs"><a href="<c:url value="/messages" />">Messages
							(<span id="numberMessages">0</span>)
					</a><span class="menu-arrow">&rsaquo;</span></li>
				</sec:authorize>

				<c:choose>
					<c:when test="${hasOffer}">
						<li class="side-tabs"><a
							href="${pageContext.request.contextPath}/ownoffers">Edit/Delete
								offer</a><span class="menu-arrow">&rsaquo;</span></li>
					</c:when>
					<c:otherwise>
						<li class="side-tabs"><a
							href="${pageContext.request.contextPath}/createoffer">Add new
								offer</a><span class="menu-arrow">&rsaquo;</span></li>
					</c:otherwise>
				</c:choose>

				<li class="side-tabs"><a
					href="${pageContext.request.contextPath}/offers">Show current
						offers.</a> <span class="menu-arrow">&rsaquo;</span></li>

				<sec:authorize access="hasRole('ROLE_ADMIN')">
					<li class="side-tabs"><a href="<c:url value='/admin' />">Admin</a>
						<span class="menu-arrow">&rsaquo;</span></li>
				</sec:authorize>

			</ul>
		</nav>
	</div>
</div>
