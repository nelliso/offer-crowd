<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/home.css" />

</head>
<body>

	<div class="header">
		<div class="header-inner-div clear">
			<div class="header-logo">
				<img src="${pageContext.request.contextPath}/static/images/logo.png"
					title="OfferCrowd Logo" />
			</div>

			<div class="header-buttons">
				<a href="#"><button class="loginBtn">Log in</button> </a>
			</div>
			
		</div>
	</div>

	<footer class="footer">
	<div class="footer-div-inner">
		<p>
			Copyright &copy; <a href="#" title="Offer Crowd"><span>Offer
					Crowd.</span></a>&nbsp;&nbsp; All Rights Reserved. &nbsp;&nbsp;Powered by MNN<sup>&trade;</sup>
		</p>
	</div>
	</footer>

</body>
</html>