package com.mnn333.spring.web.controllers;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mnn333.spring.web.dao.FormValidationGroup;
import com.mnn333.spring.web.dao.Offer;
import com.mnn333.spring.web.service.OffersService;

@Controller
public class OffersController {

	private OffersService offersService;

	@Autowired
	public void setOffersService(OffersService offersService) {
		this.offersService = offersService;
	}

	@RequestMapping("/offers")
	public String showOffers(Model model, Principal principal) {

		// offersService.throwTestExceptions();

		List<Offer> offers = offersService.getCurrent();
		model.addAttribute("offers", offers);

		boolean hasOffer = false;

		if (principal != null) {
			hasOffer = offersService.hasOffer(principal.getName());
		}

		model.addAttribute("hasOffer", hasOffer);

		return "offers";
	}
	
	@RequestMapping("/ownoffers")
	public String ownOffers(Model model, Principal principal){
		
		List<Offer> offer = null;
		
		if(principal != null){
			String name = principal.getName();
			offer = offersService.getOffer(name);
		}
	
		for(Offer offers : offer){
			System.out.println(offers);
		}
		
		model.addAttribute("ownoffer", offer);
		
		return "ownoffers";
	}

	/*@RequestMapping("/createoffer")
	public String createOffer(Model model, Principal principal) {

		Offer offer = null;

		if (principal != null) {
			String username = principal.getName();

			offer = offersService.getOffer(username);
		}

		if (offer == null) {
			offer = new Offer();
		}

		model.addAttribute("offer", offer);

		return "createoffer";
	}*/
	
	
	@RequestMapping("/createoffer")
	public String createOffer(Model model) {

		model.addAttribute("offer", new Offer());

		return "createoffer";
	}

	@RequestMapping(value = "/docreate", method = RequestMethod.POST)
	public String doCreate(Model model, @Validated(value=FormValidationGroup.class) Offer offer,
			BindingResult result, Principal principal, @RequestParam(value="delete", required=false) String delete) {

		if (result.hasErrors()) {
			return "createoffer";
		}
		
		if(delete == null){
			String username = principal.getName();
			offer.getUser().setUsername(username);
			offersService.saveOrUpdate(offer);
			model.addAttribute("message", "User added");
			//return "offercreated";
			return "home";
		}else{
			offersService.delete(offer.getId());
			return "offerdeleted";
		}

	}
}
