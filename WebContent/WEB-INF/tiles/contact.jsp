<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="main-content">
<header>
	<h2>
		<span>S</span>end Message
	</h2>
</header>

<sf:form id="details" method="post" commandName="message">

	<input type="hidden" name="_flowExecutionKey"
		value="${_flowExecutionKey}" />
	<input type="hidden" name="_eventId" value="send" />

	<table class="formtable">

		<tr>
			<td class="label">Your name:</td>
			<td class="input-td"><sf:input class="input-data" name="name"
					path="name" type="text" value="${fromName}" /><br> <sf:errors
					path="name" cssClass="error"></sf:errors></td>
		</tr>

		<tr>
			<td class="label">Email:</td>
			<td class="input-td"><sf:input class="input-data" name="email"
					path="email" type="text" value="${fromEmail}" /><br> <sf:errors
					path="email" cssClass="error"></sf:errors></td>
		</tr>

		<tr>
			<td class="label">Subject:</td>
			<td class="input-td"><sf:input class="input-data" name="subject"
					type="text" path="subject" /><br> <sf:errors path="subject"
					cssClass="error"></sf:errors></td>
		</tr>

		<tr>
			<td class="label">Content:</td>
			<td class="input-td"><sf:textarea class="input-data"
					name="content" type="text" path="content" /><br> <sf:errors
					path="content" cssClass="error"></sf:errors></td>
		</tr>

		<tr>
			<td class="label"></td>
			<td>
				<div class="button-create">
					<input value="Create Message" type="submit" />
				</div>
				<div class="button-cancel">
					<a href='<c:url value="/" />'><input value="Cancel Message" /></a>
				</div>
			</td>
		</tr>
	</table>
</sf:form>
</div>