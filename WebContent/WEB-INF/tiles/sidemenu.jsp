<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<script type="text/javascript">
<!--
/*$(function() {
		// this will get the full URL at the address bar
		var url = window.location.href;
		alert(url);
		// passes on every "a" tag
		$(".sidemenu-tabs a").each(function() {
			// checks if its the same on the address bar
			if (url == (this.href)) {
				$(this).closest("li").addClass("active");
			}
		});
	});

	function menuHighlight() {

	}*/
	
	

	$(document).ready(menuHighlight);
//-->
</script>

<div class="sidemenu-sub">
	<div class="sidemenu-title">
		<div class="sidemenu-title-div">Menu</div>
	</div>
	<div class="sidemenu-tabs">
		<nav>
			<ul id="sidemenu-ul">

				<li class="side-tabs"><a
					href="${pageContext.request.contextPath}/offers">Show current
						offers.</a> <span class="menu-arrow">&rsaquo;</span></li>

				<li class="side-tabs"><a
					href="${pageContext.request.contextPath}/createoffer">Add a new
						offer.</a> <span class="menu-arrow">&rsaquo;</span></li>

				<sec:authorize access="hasRole('ROLE_ADMIN')">
					<li class="side-tabs"><a href="<c:url value='/admin' />">Admin</a>
						<span class="menu-arrow">&rsaquo;</span></li>
				</sec:authorize>

			</ul>
		</nav>
	</div>
</div>