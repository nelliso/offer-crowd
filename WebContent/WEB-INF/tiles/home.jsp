<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<script type="text/javascript">
<!--
	$(document).ready(function() {
		/*$(".fadeOut").click(function() {
			$(this).hide().fadeIn(1000);
		});*/
		
		$('div#fadeOut').fadeIn(3000).delay(10000).fadeOut(3000);
	});
//-->
</script>

<div class="main-content">
	<header style="text-align: center;">
		<c:if test="${not empty message}">
			<%-- <c:out value="${message}" /> --%>
			<div id="fadeOut">
				Offer created. <a href="${pageContext.request.contextPath}/offers">View</a>
			</div>
		</c:if>

		<h2>
			<span>I</span>t works in 2 steps.
		</h2>
	</header>

	<div class="home_table">
		<table style="">
			<tr>
				<td class="home_img"><img alt=""
					src="${pageContext.request.contextPath}/static/images/paper_pen1M.png"></td>

				<td class="home_text clear">
					<div class="text_num">
						<span class="number">1</span>
					</div>
					<div class="text_desc">
						<span class="desc">Register, draft and submit service you
							are offering.</span>
						<p>Takes only 4 minutes</p>
					</div>

				</td>
			</tr>

			<tr>
				<td class="home_img"><img alt=""
					src="${pageContext.request.contextPath}/static/images/send_arrow1M.png"></td>

				<td class="home_text clear">
					<div class="text_num">
						<span class="number">2</span>
					</div>
					<div class="text_desc">
						<span class="desc">You will receive responses from
							interested individuals.</span>
						<p>End results: Connections and services are provided.</p>
					</div>

				</td>
			</tr>

		</table>
	</div>
</div>

